export const word = () => 'word'
export const uuid = () => '00000000-0000-0000-0000-000000000000'
export const url = () => 'https://domain.com'
export const name = () => 'John Doe'
export const text = () => 'Lorem ipsum'
export const date = () => '2020-01-01T00:00:00.0000'
export const mimeType = () => 'plain/text'
export const number = (max?: number) => 1
export const boolean = () => true
export const email = () => 'john.doe@domain.com'
export const fileName = () => 'file-name.extension'
